#!/bin/bash

#do dodania ww ansiblou roli common
#rm /etc/machine-id
#systemd-machine-id-setup

function distribution_check {
  test -e /etc/os-release && os_release='/etc/os-release' || os_release='/usr/lib/os-release'
  . "${os_release}"
}

function setting_variables {
  read -p "Set hostname: [system]: " hostname
  hostname=${hostname:-system}
  echo $hostname

  read -p "Set alternative root user name to create (root account can be disabled in next steps) [lain]: " rootusername
  rootusername=${rootusername:-lain}
  echo $rootusername

  read -p "Set $rootusername user password: " rootuserpass
  echo $rootuserpass

  read -p "Set NOPASSWD for $rootusername user (y/n)? [n]: " passaskingroot
  passaskingroot=${passaskingroot:-n}
  echo $passaskingroot

  read -p "Set automation username to create [ansible]: " automationusername
  automationusername=${automationusername:-ansible}
  echo $automationusername

  read -p "Set $automationusername user password: " automationuserpass
  echo $automationuserpass

  read -p "Set NOPASSWD for $automationusername user (y/n)? [n]: " passaskingautomation
  passaskingautomation=${passaskingautomation:-n}
  echo $passaskingautomation

  read -p "Do you want to configure network (y/n) (remote session will be ended)? [n]: " networkconfigure
  networkconfigure=${networkconfigure:-n}
  echo $networkconfigure

  if [ $networkconfigure == "y" ]
  then
    read -p "Set gateway [192.168.1.1]: " gateway
    gateway=${gateway:-192.168.1.1}
    echo $gateway

    read -p "Set IP [192.168.0.10]: " ipaddress
    ipaddress=${ipaddress:-192.168.0.10}
    echo $ipaddress

    read -p "Set subnet mask [23]: " subnetmask
    subnetmask=${subnetmask:-23}
    echo $subnetmask

    read -p "Set DNS first server [192.168.1.1]: " firstdns
    firstdns=${firstdns:-192.168.1.1}
    echo $firstdns

    read -p "Set DNS sec. server: [8.8.8.8]: " secdns
    secdns=${secdns:-8.8.8.8}
    echo $secdns
  fi

  read -p "Do you want to disable standard root account? (preffered)(y/n)? [y]: " rootuserlocked
  rootuserlocked=${qemuagentinstalled:-y}
  echo $rootuserlocked

  read -p "Do you want to permit root login (y/n/prohibit-password)? [n]: " permitrootloginanswer
  permitrootloginanswer=${permitrootloginanswer:-n}
  echo $permitrootloginanswer

  read -p "Do you want to copy public key to $rootusername ssh? (y/n)? [y]: " copysshkeyrootanswer
  copysshkeyrootanswer=${copysshkeyrootanswer:-y}
  echo $copysshkeyrootanswer
  
  read -p "Do you want to reboot system after configuration? (poweroff/reboot/nothing)? [nothing]: " afterconfiguration
  afterconfiguration=${afterconfiguration:-nothing}
  echo $afterconfiguration
}

function pre_configuration {
  echo "pre-configuration tasks."
  useradd -m "$rootusername"
  useradd -m "$automationusername"
}

function cross_system_configuration {
  hostnamectl set-hostname $hostname
  if [ "$passaskingroot" == y ]
    then
      echo "$rootusername ALL=NOPASSWD: ALL" > /etc/sudoers.d/$rootusername
  fi
  if [ "$passaskingautomation" == y ]
    then
      echo "$automationusername ALL=NOPASSWD: ALL" > /etc/sudoers.d/$automationusername
  fi
  echo $rootusername:$rootuserpass | chpasswd
  echo $automationusername:automationuserpass | chpasswd
  if [ $rootuserlocked == y ]
    then
  passwd -l root
  passwd -S root
  fi
  if [ "$permitrootloginanswer" == y ]
    then
      sed -i "/^[^#]*PermitRootLogin[[:space:]]no/c\PermitRootLogin yes" /etc/ssh/sshd_config
      sed -i "/^[^#]*PermitRootLogin[[:space:]]prohibit-password/c\PermitRootLogin yes" /etc/ssh/sshd_config
  fi
  if [ "$permitrootloginanswer" == n ]
    then
      sed -i "/^[^#]*PermitRootLogin[[:space:]]yes/c\PermitRootLogin no" /etc/ssh/sshd_config
      sed -i "/^[^#]*PermitRootLogin[[:space:]]prohibit-password/c\PermitRootLogin no" /etc/ssh/sshd_config
  fi
  if [ "$permitrootloginanswer" == prohibit-password ]
    then
      sed -i "/^[^#]*PermitRootLogin[[:space:]]no/c\PermitRootLogin prohibit-password" /etc/ssh/sshd_config
      sed -i "/^[^#]*PermitRootLogin[[:space:]]yes/c\PermitRootLogin prohibit-password" /etc/ssh/sshd_config
  fi
  if [ "$copysshkeyrootanswer" == y ]
    then
      mkdir -p /home/$rootusername/.ssh
      touch /home/$rootusername/.ssh/authorized_keys
      echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC9twauuoZKiwHHQoHq7TDsQYrlx29HMHliY6W6PKuLzWBgqWMU/4UtRUa+TEHufjhIgUOH9h3gvKfnIHO04BxPxr78ZeekoJ8IwiOnHPT1h5EKi+KfXyuUiDSxX5YsfVJpbsA0fKX6MrNsOmQn5aypUtXzV62E4TtrRbsqByfEH13TmJG+B+ObjWId1qQcogQiAxeFFPD25oufo3dlV1gtSxwOclJB+ZjY9gFIF2+wwuBhdRpWOxLNLlVFkFllyD+78nsp2B+81Okki7QFwpw4L9wKjKjr6qK9cKnY8bRS6hzHAfCUEx6Mf/hziAX/NXYK4TgqQH3hPI+noVQkQnYbmiyHXFh3PKtlp9UwNMBQAUwji082/El3hCpuFiWg5oxnPYS55VXOTg5FmC9ZZTJ3ywsNfSAAeku/KgzffFw62uGC5isVquhkF3loYzb1HDC41pzv458mXRVLaWXeD8WZMHt9yw6UwnW6iIStX+J3Xha8MmQBeL1AuAcIFHMyw1s= soda2@galaxy0.wired.sytes.net" >> /home/$rootusername/.ssh/authorized_keys
  fi
  systemctl restart sshd
}

function arch_system_configuration {
  pacman -Syu
  pacman -Sy qemu-guest-agent python3 openssh networkmanager
  systemctl enable --now sshd
  systemctl enable --now qemu-guest-agent
  systemctl enable --now NetworkManager
  systemctl status sshd
  systemctl status qemu-guest-agent
  usermod -aG wheel $rootusername
  usermod -aG wheel $automationusername
}

function enterprise_system_configuration {
  dnf update -y
  dnf install -y qemu-guest-agent python3 openssh-server NetworkManager-tui
  systemctl enable --now sshd
  systemctl enable --now qemu-guest-agent
  systemctl status sshd
  systemctl status qemu-guest-agent
  usermod -aG wheel $rootusername
  usermod -aG wheel $automationusername
}

function fedora_system_configuration {
  dnf update -y
  dnf install -y qemu-guest-agent python3 openssh-server NetworkManager-tui
  systemctl enable --now sshd
  systemctl enable --now qemu-guest-agent
  systemctl status sshd
  systemctl status qemu-guest-agent
  usermod -aG wheel $rootusername
  usermod -aG wheel $automationusername
}

function debian_system_configuration {
  apt-get update
  apt-get upgrade
  apt-get install qemu-guest-agent sudo python3 openssh-server netmask sudo
  systemctl status sshd
  systemctl status qemu-guest-agent
  usermod -aG sudo $rootusername
  usermod -aG sudo $automationusername
}

function ubuntu_system_configuration {
  apt-get update
  apt-get upgrade
  apt-get install qemu-guest-agent python3 openssh-server netmask sudo
  systemctl status sshd
  systemctl status qemu-guest-agent
  usermod -aG sudo $rootusername
  usermod -aG sudo $automationusername
}

function arch_network_configuration {
  nmcli connection delete "Wired connection 1"
  nmcli con add type ethernet con-name static1 ifname enp1s0 ip4 $ipaddress/$subnetmask gw4 $gateway
  nmcli con mod static1 ipv4.dns "$firstdns $secdns"
  nmcli con reload
}

function enterprise_network_configuration {
  nmcli connection delete "Wired connection 1"
  nmcli con add type ethernet con-name static1 ifname enp1s0 ip4 $ipaddress/$subnetmask gw4 $gateway
  nmcli con mod static1 ipv4.dns "$firstdns $secdns"
  nmcli con reload
}

function fedora_network_configuration {
  nmcli connection delete "Wired connection 1"
  nmcli con add type ethernet con-name static1 ifname enp1s0 ip4 $ipaddress/$subnetmask gw4 $gateway
  nmcli con mod static1 ipv4.dns "$firstdns $secdns"
  nmcli con reload
}

function debian_network_configuration {
  #converting netmask from CIDR to dotted address format
  convertednetmask=$(netmask -s $ipaddress/$subnetmask)

  #split resoult of netmask to the decimal mask
 
  IFS='/'     # delimiter
  read -ra ADDR <<< "$convertednetmask"   # str is read into an array as tokens separated by IFS
  IFS=' '     # reset to default value after usage

  #remove old configuration
  rm /etc/network/interfaces
  #setting up
  cat >> /etc/network/interfaces <<EOL
  # This file describes the network interfaces available on your system
  # and how to activate them. For more information, see interfaces(5).
  
  source /etc/network/interfaces.d/*
  
  # The loopback network interface
  auto lo
  iface lo inet loopback

  # The primary network interface
  ###allow-hotplug ens18
  ###iface ens18 inet dhcp

  # The primary network interface
  auto ens18
  iface ens18  inet static
   address $ipaddress
   netmask ${ADDR[1]}
   gateway $gateway
   dns-domain wired.sytes.net
   dns-nameservers $firstdns $secdns
EOL

  #network restart
  systemctl restart systemd-networkd
}

function ubuntu_network_configuration {
  #usunąć stare konfiguracje
  rm -rf /etc/netplan/*

  #setting up
  cat >> /etc/netplan/static1.yaml <<EOL
# This is the network config written by 'subiquity'
#network:
#  ethernets:
#    ens18:
#      dhcp4: true
#  version: 2

network:
  version: 2
  renderer: networkd
  ethernets:
    ens18:
      dhcp4: false
      dhcp6: false
      addresses:
      - 192.168.0.204/23
      routes:
      - to: default
        via: 192.168.1.1
      nameservers:
        addresses:
        - 192.168.1.1
        - 8.8.8.8
EOL

netplan apply

#network restart
systemctl restart systemd-networkd

}

function post_configuration {
  echo "post-configuration tasks."
  logger -s "That system is succesfully configurated by bootstrap script." 
  if [ $afterconfiguration == "poweroff" ]
  then
    poweroff
  fi
  if [ $afterconfiguration == "reboot" ]
  then
    reboot
  fi
  id $rootusername
  id $automationusername
}

echo "========================================================================================================================================"
echo "                                               bootstrap script | version: 1.0"
echo "                                       that script configures supported linux systems"
echo "========================================================================================================================================"
echo "                                             please run that script as root or with sudo."
echo "       if you run that script by remote connection probably your connection will be disrupted during network configuration."
echo "that script removes all previous configuration of network in debian and ubuntu. In rhel and fedora script removes default connection only."
echo "  please use rhel/fedora/debian/ubuntu. distro is detected by ID variable value in /etc/os-release (Linux Standard Base is required.)"
echo "                    if you want to set public key to copy please edit that script before you use it."
echo "========================================================================================================================================"
distribution_check
setting_variables
pre_configuration

case $ID in

  almalinux | rocky | centos)
    echo " >>> enterprise linux is detected. <<<<<<<< "
    enterprise_system_configuration
    cross_system_configuration
    if [ $networkconfigure == "y" ]
    then
      enterprise_network_configuration
    fi
    ;;

  fedora)
    echo " >>> fedora linux is detected. <<<<<<<< "
    fedora_system_configuration
    cross_system_configuration
    if [ $networkconfigure == "y" ]
    then
      fedora_network_configuration
    fi
    ;;

  debian)
    echo " >>> debian linux is detected. <<<<<<<< "
    debian_system_configuration
    cross_system_configuration
    if [ $networkconfigure == "y" ]
    then
      debian_network_configuration
    fi
    ;;

  ubuntu)
    echo " >>> ubuntu linux is detected. <<<<<<<< "
    ubuntu_system_configuration
    cross_system_configuration
    if [ $networkconfigure == "y" ]
    then
      ubuntu_network_configuration
    fi
    ;;

  arch)
    echo " >>> arch linux is detected. <<<<<<<"
    arch_system_configuration
    cross_system_configuration
    if [ $networkconfigure == "y" ]
    then
      arch_network_configuration
    fi
    ;;

  *)
    echo -n "XXXXXXXX unknown or unsupported distribution. Please use rhel, fedora, debian or ubuntu. XXXXXXXX"
    ;;
esac

post_configuration
